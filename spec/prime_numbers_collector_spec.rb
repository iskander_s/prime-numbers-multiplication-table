require_relative '../lib/prime_numbers_collector.rb'

describe PrimeNumbersCollector do
  context "given the amount N of expected prime numbers" do
    it "collects first N prime numbers" do
      numbers = 5
      prime_numbers_collector = PrimeNumbersCollector.new(numbers)

      expect(prime_numbers_collector.result).to eq [2, 3, 5, 7, 11]
    end
  end

  context "amount of expected prime numbers is not given" do
    it "takes first 10 prime numbers" do
      prime_numbers_collector = PrimeNumbersCollector.new

      expect(prime_numbers_collector.result).to eq [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]
    end
  end
end
