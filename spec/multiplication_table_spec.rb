require_relative '../lib/multiplication_table.rb'

describe MultiplicationTable do
  it "generates a multiplication table" do
    numbers = [3,5]
    multiplication_table = MultiplicationTable.new(numbers)

    expect(multiplication_table.cells[0][0]).to eq 0
    expect(multiplication_table.cells[0][1]).to eq numbers[0]
    expect(multiplication_table.cells[0][2]).to eq numbers[1]
    expect(multiplication_table.cells[1][0]).to eq numbers[0]
    expect(multiplication_table.cells[2][0]).to eq numbers[1]
    expect(multiplication_table.cells[1][1]).to eq numbers[0] * numbers[0]
    expect(multiplication_table.cells[2][1]).to eq numbers[0] * numbers[1]
    expect(multiplication_table.cells[1][2]).to eq numbers[1] * numbers[0]
    expect(multiplication_table.cells[2][2]).to eq numbers[1] * numbers[1]
  end

  it "prints the table" do
    numbers = [3,5,7]
    multiplication_table = MultiplicationTable.new(numbers)

    expect do
      multiplication_table.print
    end.to output("0 3 5 7\n3 9 15 21\n5 15 25 35\n7 21 35 49\n").to_stdout
  end
end
