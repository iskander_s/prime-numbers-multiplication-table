require_relative '../lib/ext/integer.rb'

describe Integer do
  it "checks if the number is prime or not" do
    prime_num = 13
    not_prime_num = 9

    expect(prime_num.is_prime?).to be true
    expect(not_prime_num.is_prime?).to be false
  end
end
