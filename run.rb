require_relative 'lib/prime_numbers_collector.rb'
require_relative 'lib/multiplication_table.rb'

puts "Enter table size:"
table_size = gets.chomp
prime_numbers_collector = table_size.empty? ? PrimeNumbersCollector.new : PrimeNumbersCollector.new(table_size.to_i)
multiplication_table = MultiplicationTable.new(prime_numbers_collector.result)
multiplication_table.print
