require_relative 'ext/integer.rb'

class PrimeNumbersCollector
  DEFAULT_TABLE_SIZE = 10
  attr_reader :result

  def initialize(amount = DEFAULT_TABLE_SIZE)
    i = 2
    @result = []
    while @result.length < amount
      @result << i if i.is_prime?
      i += 1
    end
  end
end
