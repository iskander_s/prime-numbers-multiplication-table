class Integer
  def is_prime?
    return false if self < 2
    (2..Math.sqrt(self)).none? { |i| self % i == 0 }
  end
end
