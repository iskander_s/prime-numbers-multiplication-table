class MultiplicationTable
  attr_reader :cells

  def initialize(numbers)
    rows_size = cols_size = numbers.length + 1
    @cells = Array.new(rows_size) { Array.new(cols_size) }
    fill_table(numbers)
  end

  def print
    @cells.each{ |row| puts row.join(" ") }
  end

  private def fill_table(numbers)
    @cells[0][0] = 0
    i = 1
    while i <= numbers.length
      @cells[i][0] = numbers[i - 1]
      @cells[0][i] = numbers[i - 1]
      j = 1
      while j <= i
        @cells[i][j] = @cells[0][i] * @cells[j][0]
        @cells[j][i] = @cells[i][j]
        j += 1
      end
      i += 1
    end
  end
end
